package com.example.emailsender.exception.email;

import com.example.emailsender.exception.AbstractException;

public class EmailAlreadySubscribedException extends AbstractException {

    public EmailAlreadySubscribedException() {
        super("Email is already subscribed!");
    }

}
