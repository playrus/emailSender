package com.example.emailsender.exception.email;

import com.example.emailsender.exception.AbstractException;

public class EmailEmptyException extends AbstractException {

    public EmailEmptyException() {
        super("Email is empty!");
    }

}
