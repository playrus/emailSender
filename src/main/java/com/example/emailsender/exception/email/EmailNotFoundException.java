package com.example.emailsender.exception.email;

import com.example.emailsender.exception.AbstractException;

public class EmailNotFoundException extends AbstractException {

    public EmailNotFoundException() {
        super("Email is not found!");
    }

}
