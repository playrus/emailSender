package com.example.emailsender.handler;

import com.example.emailsender.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class EmailSenderHandler implements WebSocketHandler {

    private final NotificationService notificationService;

    @Override
    public Mono<Void> handle(WebSocketSession webSocketSession) {
        return webSocketSession.send(webSocketSession.receive()
                .map(WebSocketMessage::getPayloadAsText)
                .flatMap(notificationService::sendMessage)
                .map(us-> webSocketSession.textMessage("")));
    }

}
