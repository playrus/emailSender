package com.example.emailsender.handler;

import com.example.emailsender.model.Subscriber;
import com.example.emailsender.service.SubscriberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class SubscriberDeleteHandler implements WebSocketHandler {

    private final SubscriberService subscriberService;

    @Override
    public Mono<Void> handle(WebSocketSession webSocketSession) {
        return webSocketSession.send(webSocketSession.receive()
                .map(WebSocketMessage::getPayloadAsText)
                .flatMap(subscriberService::deleteByEmail)
                .map(us-> webSocketSession.textMessage(subscriberService.getAllSubscribers()
                        .map(Subscriber::getId).toString())));
    }

}
