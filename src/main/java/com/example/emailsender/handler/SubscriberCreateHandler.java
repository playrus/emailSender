package com.example.emailsender.handler;

import com.example.emailsender.model.Subscriber;
import com.example.emailsender.service.SubscriberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class SubscriberCreateHandler implements WebSocketHandler {

    private final SubscriberService subscriberService;

    @Override
    public Mono<Void> handle(WebSocketSession webSocketSession) {
        return webSocketSession.send(subscriberService.getAllSubscribers().map(Subscriber::getEmail)
                        .map(webSocketSession::textMessage))
                .and(webSocketSession.receive()
                        .map(email -> new Subscriber(email.getPayloadAsText()))
                        .flatMap(subscriberService::save));
    }

}
