package com.example.emailsender.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@NoArgsConstructor
@Document(collection = "app_subscriber")
public class Subscriber {

    @Id
    private String id = UUID.randomUUID().toString();

    private String email;

    public Subscriber(String email) {
        this.email = email;
    }

}
