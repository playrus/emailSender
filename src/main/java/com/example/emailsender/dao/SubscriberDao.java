package com.example.emailsender.dao;

import com.example.emailsender.model.Subscriber;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface SubscriberDao extends ReactiveMongoRepository<Subscriber, String> {

    Mono<Subscriber> findByEmail(final String email);

    Mono<Void> deleteByEmail(final String email);

}
