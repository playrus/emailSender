package com.example.emailsender.service;

import com.example.emailsender.model.Subscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SubscriberService {

    Flux<Subscriber> getAllSubscribers();

    Mono<Void> deleteByEmail(String email);

    Mono<Subscriber> save(Subscriber subscriber);

}
