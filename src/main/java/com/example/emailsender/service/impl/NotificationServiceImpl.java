package com.example.emailsender.service.impl;

import com.example.emailsender.dao.SubscriberDao;
import com.example.emailsender.model.Subscriber;
import com.example.emailsender.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {

    private final JavaMailSender emailSender;

    private final SubscriberDao subscriberDao;

    @Value("${spring.mail.username}")
    private String from;


    @Override
    public Mono<Void> sendMessage(String text) {
        String[] split = text.split("&7123");
        var message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(Objects.requireNonNull(subscriberDao.findAll()
                        .map(Subscriber::getEmail)
                        .collectList()
                        .toFuture().join())
                .toArray(String[]::new));
        message.setSubject(split[0]);
        message.setText(split[1]);
        emailSender.send(message);
        return null;
    }
}
