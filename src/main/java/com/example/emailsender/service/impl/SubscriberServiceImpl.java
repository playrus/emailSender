package com.example.emailsender.service.impl;

import com.example.emailsender.dao.SubscriberDao;
import com.example.emailsender.exception.email.EmailAlreadySubscribedException;
import com.example.emailsender.exception.email.EmailEmptyException;
import com.example.emailsender.exception.email.EmailNotFoundException;
import com.example.emailsender.model.Subscriber;
import com.example.emailsender.service.SubscriberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class SubscriberServiceImpl implements SubscriberService {

    private final SubscriberDao subscriberDAO;

    @Override
    public Flux<Subscriber> getAllSubscribers() {
        return subscriberDAO.findAll();
    }

    @Override
    public Mono<Void> deleteByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (Boolean.FALSE.equals(subscriberDAO.findByEmail(email).hasElement().toFuture().join()))
            throw new EmailNotFoundException();
        return subscriberDAO.deleteByEmail(email);
    }

    @Override
    public Mono<Subscriber> save(final Subscriber subscriber) {
        if (subscriber.getEmail() == null || subscriber.getEmail().isEmpty()) throw new EmailEmptyException();
        if (Boolean.TRUE.equals(subscriberDAO.findByEmail(subscriber.getEmail()).hasElement().toFuture().join()))
            throw new EmailAlreadySubscribedException();
        return subscriberDAO.save(subscriber);
    }

}
