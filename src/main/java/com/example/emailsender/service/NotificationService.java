package com.example.emailsender.service;

import reactor.core.publisher.Mono;

public interface NotificationService {

    Mono<Void> sendMessage(String text);

}
