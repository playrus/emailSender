let wsSubscribe = null;
let wsDelete = null;
let wsSend = null;

let emails = [];

function createWS(callback, address) {
    wsSubscribe = new WebSocket("ws://" + address + "/subscribers");
    wsDelete = new WebSocket("ws://" + address + "/delete");
    wsSend = new WebSocket("ws://" + address + "/send");
    wsSubscribe.onmessage = function(event) {
        events(event.data, callback);
    };
}

function connect() {
        let address = document.getElementById('WSAddress').value;
        createWS(createListener, address);
    wsSend.onerror = () => {
        alert('Error!');
    }
    wsSend.onopen = () => {
        alert('Connected successfully!')
        document.getElementById('connect').hidden = true;
        document.getElementById('content').hidden = false;
    }
}

function events(email, callback) {
    document.querySelector(".tbody").innerHTML +=
        '<tr id="' + email + '">' +
        '<td>' + email + '</td>' +
        '<td class="text-right">'+
        '<div class="btn-group pull-right">' +
        '<button class="btn btn-outline-secondary edit-button" type="button" name="'+ email + '">Edit</button>' +
        '<button class="btn btn-outline-secondary delete-button" type="button" name="'+ email + '">Delete</button>' +
        '</div>' +
        '</td>' +
        '</tr>';
    emails.push(email);
    callback();
}

function createListener() {
    let deleteButtonList = document.getElementsByClassName('delete-button')
    for (let item of deleteButtonList) {
        item.addEventListener("click", deleteEmail(item), false);
    }

    let editButtonList = document.getElementsByClassName('edit-button')
    for (let item of editButtonList) {
        item.addEventListener("click", editEmail(item), false);
    }
}

function editEmail(button) {
    return function (e) {
        return edit(e, button)
    }
    function edit(e, button) {
        let el = document.getElementById(button.name).querySelector("td");
        if (isEditable(el)) {
            el.removeAttribute('contenteditable');
            changeEmail(button);
            button.textContent = "Edit";
        } else {
            el.setAttribute('contenteditable','')
            button.textContent = "Save";
            el.focus();
        }
    }
}

function isEditable(el) {
    return el.getAttribute('contenteditable') != null;
}

function changeEmail(button) {
    let newEmail = document.getElementById(button.name).querySelector("td").innerText;
    if (newEmail === button.name || newEmail == null || newEmail === "") return;
    wsDelete.send(button.name)
    document.getElementById(button.name).id = newEmail;
    let deleteButton = document.getElementsByClassName('delete-button').namedItem(button.name);
    deleteButton.name = newEmail;
    button.name = newEmail;
    wsSubscribe.send(newEmail);
}

function deleteEmail(button) {
    return function (e) {
        return del(e, button)
    }
    function del(e, button) {
        wsDelete.send(button.name)
        document.getElementById(button.name).remove()
    }
}

function checkEmail(email) {
    if (email == null || email === "") return false
    return !emails.includes(email);
}

function subscribe() {
    let email = document.getElementById('email').value;
    if (checkEmail(email)) {
        wsSubscribe.send(email);
        events(email)
        document.getElementById('msg').innerHTML = '<span style="color:green; font-weight: bolder; margin-top: 5px;">Successfully subscribed: ' + email + '</span>';
    } else {
        document.getElementById('msg').innerHTML = '<span style="color:red; font-weight: bolder; margin-top: 5px;">Email is empty or already added</span>';
    }
}

function deleteSubscriber() {
    let emailForDelete = document.getElementById('email').value;
    wsDelete.send(emailForDelete);
    document.getElementById(emailForDelete).remove();
    document.getElementById('msg').innerHTML = '<span style="color:green; font-weight: bolder; margin-top: 5px;">Successfully delete: ' + emailForDelete + '</span>';
}

function sendEmail() {
    let subject = document.getElementById('subjectItem').value;
    let message = document.getElementById('textMessage').value;
    wsSend.send(subject + "&7123" + message);
    alert("Message sent!");
}